package hocine.triki.configuration;

import java.util.Optional;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.arc.config.ConfigProperties;

@ConfigProperties(prefix = "greeting")
public interface IConfiguration {

    @ConfigProperty(name = "message") 
    String message();

    @ConfigProperty(defaultValue = "!")
    String getSuffix(); 

    Optional<String> getName(); 
    
    // --> javax.enterprise.inject.spi.DeploymentException
    //@ConfigProperty(name = "test")
    //String test();
}
