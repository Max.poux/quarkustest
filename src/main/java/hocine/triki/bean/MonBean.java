package hocine.triki.bean;

public class MonBean {

	public String nom;
	public String prenom;
	public Integer age;
	
	public MonBean() {
		super();
	}
	
	public MonBean(String nom, String prenom, Integer age) {
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}
}
