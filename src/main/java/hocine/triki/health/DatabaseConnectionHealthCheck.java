package hocine.triki.health;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;


// Accessible depuis l'adresse /health/ready
// Accessible aussi depuis /health
// Indique si un service en particulier répond ou pas, par exemple l'accès à une base de données.
@Readiness
@ApplicationScoped
public class DatabaseConnectionHealthCheck implements HealthCheck {

	@Override
	public HealthCheckResponse call() {
		return HealthCheckResponse.up("Database connection health check");
	}

}
