
TEST QUARKUS

run de l'application
	sudo ./mvnw compile quarkus:dev -Dqarkus.profile=dev

Ajout de quarkus-resteasy-jackson pour le support de Jackson
	./mvnw quarkus:add-extension -Dextensions="io.quarkus:quarkus-resteasy-jackson"

		    <dependency>
		      <groupId>io.quarkus</groupId>
		      <artifactId>quarkus-resteasy-jackson</artifactId>
		    </dependency> 

    --> possibilité de customiser le mapping via un Singleton .... https://quarkus.io/guides/rest-json-guide
    
Ajout de swagger (uniquement dans le mode dev ou test) -> actif depuis l'url :  /swagger-ui
	./mvnw quarkus:add-extension -Dextensions="openapi"
	ajout dans le fichier de properties : quarkus.swagger-ui.always-include=true
	

Ajout l'API de secours :

	./mvnw quarkus:add-extension -Dextensions="health"

	